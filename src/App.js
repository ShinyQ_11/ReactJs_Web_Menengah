import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import DataJSON from './list';

class App extends Component {

  constructor(props){

    super(props);
    this.state = {
      list: DataJSON
    }

    this.hapusItem = this.hapusItem.bind(this);
  }

  hapusItem(id){
    console.log('Hapus Iem')
  }

  render() {
    return (
      <div className="App">
          {
            this.state.list.map(function(item){
              return(
                <div key={item.id}>
                <h1>{item.judul}</h1>
                <h3>{item.bahasa}</h3>
                <h3>{item.produser}</h3>
                <h3>{item.tahun}</h3>
                <button onClick={ ()=> this.hapusItem(item.id) }>Hapus</button>
                </div>
              )
            })
          }
      </div>
    );
  }
}

export default App;
